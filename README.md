## uki-webstack-tag-enforcer

### Create .awsenv

- AWS_DEFAULT_REGION - Region where resources are
- AWS_ROLE_ARN - Role we are assuming, this should be your user's role in the account you wish to target
- AWS_ROLE_SESSION_NAME - Set this to the role session name, not too important e.g tag-enforcer
- AWS_MFA_SERIAL - Serial number of your MFA device, this can be found in AWS
- AWS_PROFILE - The profile name from your ~/.aws/credentials file e.g mine is named tui

#### Example .awsenv file

AWS_DEFAULT_REGION="eu-central-1"
AWS_ROLE_ARN="arn:aws:iam::<ACCOUNT_NUMBER>:role/<ROLE_NAME>"
AWS_ROLE_SESSION_NAME="tag-enforcer"
AWS_MFA_SERIAL="<ARN_OF_MFA_DEVICE>"
AWS_PROFILE="tui"
