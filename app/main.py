import os
import boto3

aws_region = os.getenv('AWS_DEFAULT_REGION', 'eu-central-1')
ec2 = boto3.client('ec2', region_name=aws_region)
rds = boto3.client('rds', region_name=aws_region)

"""
Convert a list of tags to aws filter format
"""
def convert_tags_to_filters(tags):
    filters = []

    for tag in tags:
        name = tag['Name']
        values = tag['Values']

        filter = {
            'Name': 'tag:{}'.format(name),
            'Values': values
        }

        filters.append(filter)

    return filters

"""
Get ec2 instances with specified tags and return a list of instance objects
"""
def ec2_instances_by_tags(tags):
    instances = []
    filters = convert_tags_to_filters(tags)

    response = ec2.describe_instances(Filters=filters)

    for reservation in response['Reservations']:
        for instance in reservation['Instances']:
            instances.append(instance)

    return instances

"""
Extract instance id from list of instance objects and return a list
of instance ids
"""
def ec2_extract_instance_ids(instances):
    instance_ids = []

    for instance in instances:
        instance_id = instance['InstanceId']

        instance_ids.append(instance_id)

    return instance_ids

"""
Create tags on an instance by its instance id
"""
def ec2_create_tags_by_instance_id(instance_id, tags):
    ec2.create_tags(
        Resources=[
            instance_id
        ],
        Tags=tags
    )

"""
Filter rds resource arns by the supplied tags
"""
def rds_resource_arns_by_tags(arns, tags):
    resource_arns = []

    for arn in arns:
        response = rds.list_tags_for_resource(ResourceName=arn)
        tag_list = response['TagList']

        for tag in tags:
            for resource_tag in tag_list:
                if tag['Name'] == resource_tag['Key']:
                    for tag_value in tag['Values']:
                        if tag_value == resource_tag['Value']:
                            if arn not in resource_arns:
                                resource_arns.append(arn)

    return resource_arns

"""
Get rds clusters with specified tags and return a list of rds cluster arns
"""
def rds_cluster_arns_by_tags(tags):
    all_cluster_arns = []
    filtered_cluster_arns = []

    response = rds.describe_db_clusters()

    for cluster in response['DBClusters']:
        rds_cluster_arn = cluster['DBClusterArn']
        all_cluster_arns.append(rds_cluster_arn)

    filtered_cluster_arns = rds_resource_arns_by_tags(all_cluster_arns, tags)

    return filtered_cluster_arns

"""
Get rds instances with specified tags and return a list of rds instance arns
"""
def rds_instance_arns_by_tags(tags):
    all_instance_arns = []
    filtered_instance_arns = []

    response = rds.describe_db_instances()

    for instance in response['DBInstances']:
        rds_instance_arn = instance['DBInstanceArn']
        all_instance_arns.append(rds_instance_arn)

    filtered_cluster_arns = rds_resource_arns_by_tags(all_instance_arns, tags)

    return filtered_cluster_arns

"""
Add tags to an rds resource based on it's arn
"""
def rds_resource_create_tags_by_arn(arn, tags):
    rds.add_tags_to_resource(
        ResourceName=arn,
        Tags=tags
    )

def main():
    tags = [
        {
            "Name": 'Environment',
            "Values": ['st07']
        }
    ]

    instances = ec2_instances_by_tags(tags)
    instance_ids = ec2_extract_instance_ids(instances)
    rds_cluster_arns = rds_cluster_arns_by_tags(tags)
    rds_instance_arns = rds_instance_arns_by_tags(tags)

    print(instance_ids)
    print(rds_cluster_arns)
    print(rds_instance_arns)

if __name__ == '__main__':
    main();
